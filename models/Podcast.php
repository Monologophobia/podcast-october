<?php namespace Monologophobia\Podcasts\Models;

use \October\Rain\Database\Model;

class Podcast extends Model {

    public $table = 'mono_podcasts';

    public $timestamps = true;

    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'title' => 'required|string',
        'published_at' => 'required'
    ];

    public $belongsTo = [
        'user' => ['Backend\Models\User', 'key' => 'user_id']
    ];

    public $attachOne = [
        'audio' => ['System\Models\File']
    ];

    /**
     * Returns the length of the audio file in m:ss format
     * @return String m:ss
     */
    public function getLength() {
        // TODO: this
        // then add 
        // <itunes:duration>{{ item.getLength() }}</itunes:duration>
        // to each <item> in components/rss/default.htm
        return '0:00';
    }

    /**
     * Returns the size of the audio file in bytes
     * @return String byte size
     */
    public function getSize() {
        return $this->audio->sizeToString();
    }

    /**
     * Returns the type of the audio file
     * @return String audio/mpeg or similar
     */
    public function getType() {
        return $this->audio->getContentType();
    }

}