<?php namespace Monologophobia\Podcasts\Components;

use Lang;

use Monologophobia\Podcasts\Models\Podcast;
use Monologophobia\Podcasts\Models\Settings;

class RSS extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name' => Lang::get('monologophobia.podcasts::lang.rss.name'),
            'description' => Lang::get('monologophobia.podcasts::lang.rss.description')
        ];
    }


    public function onRun() {

        $this->page['settings'] = Settings::instance();
        $this->page['base_url'] = url('/');
        $this->page['podcasts'] = Podcast::get();

        // override October display to force response type to be rss compatible
        $content = $this->renderPartial('@default');
        return \Response::make($content)->header('Content-Type', 'application/rss+xml');
        die();

    }
}
