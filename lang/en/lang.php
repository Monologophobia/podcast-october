<?php

return [
    'podcasts' => [
        'name' => 'Podcasts',
        'singular' => 'Podcast',
        'description' => 'Published Podcasts via CMS',
        'permissions' => 'Manage Podcasts',
        'settings_description' => 'Manage Podcast Settings',
        'new' => 'New',
        'edit' => 'Edit',
        'delete_confirm' => 'Are you sure you want to delete these Podcasts?',
        'save' => 'Save and Close',
        'saving' => 'Saving...',
        'saved' => 'Saved',
        'cancel' => 'Cancel',
        'return' => 'Return to Podcasts'
    ],
    'podcast' => [
        'audio' => 'Audio File',
        'title' => 'Title',
        'description' => 'Description',
        'image' => 'Image',
        'published_at' => 'Published',
        'created_at' => 'Created',
        'keywords' => 'Keywords (iTunes)',
        'explicit' => 'Explicit',
        'user' => 'User'
    ],
    'settings' => [
        'title' => 'Title',
        'sub_title' => 'Sub Title (iTunes)',
        'image' => 'Image',
        'site_url' => 'Site URL',
        'description' => 'Description',
        'language' => 'Language',
        'owner_name' => 'Owner Name',
        'owner_email' => 'Owner Email',
        'keywords' => 'Keywords (iTunes)',
        'category' => 'Category (iTunes)',
        'explicit' => 'Explicit',
        'explicit_yes' => 'Yes',
        'explicit_no'  => 'No',
    ],
    'rss' => [
        'name' => 'Podcast RSS Feed',
        'description' => 'Display an RSS feed for published podcasts'
    ]
];