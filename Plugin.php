<?php namespace Monologophobia\Podcasts;

use Lang;
use Event;

use Backend\Facades\Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase {

    public $require = [
        'RainLab.Blog'
    ];

    public function pluginDetails() {
        return [
            'name'        => Lang::get('monologophobia.podcasts::lang.podcasts.name'),
            'description' => Lang::get('monologophobia.podcasts::lang.podcasts.description'),
            'author'      => 'Monologophobia',
            'icon'        => 'icon-podcast'
        ];
    }

    public function registerPermissions() {
        return [
            'monologophobia.podcasts' => ['tab' => 'CMS', 'label' => Lang::get('monologophobia.podcasts::lang.podcasts.permissions')]
        ];
    }

    public function registerComponents() {
        return [
           '\Monologophobia\Podcasts\Components\RSS' => 'rss'
        ];
    }

    public function boot() {

        // extend CMS to include Podcasts
        Event::listen('backend.menu.extendItems', function($manager) {

            $manager->addSideMenuItems('RainLab.Blog', 'blog', [
                'podcasts' => [
                    'label' => Lang::get('monologophobia.podcasts::lang.podcasts.name'),
                    'url'   => Backend::url('monologophobia/podcasts/podcasts'),
                    'icon'  => 'icon-podcast'
                ]
            ]);

        });

    }

    public function registerSettings() {
        return [
            'podcasts' => [
                'label'       => Lang::get('monologophobia.podcasts::lang.podcasts.name'),
                'description' => Lang::get('monologophobia.podcasts::lang.podcasts.settings_description'),
                'category'    => 'CMS',
                'icon'        => 'icon-podcast',
                'class'       => 'Monologophobia\Podcasts\Models\Settings',
                'order'       => 400,
                'permissions' => ['monologophobia.podcasts']
            ],
        ];
    }

}
