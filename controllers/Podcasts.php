<?php namespace Monologophobia\Podcasts\Controllers;

use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\Podcasts\Models\Podcast;

class Podcasts extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('RainLab.Blog', 'blog', 'podcasts');
    }

    public function index_onDelete() {
        try {
            if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
                foreach ($checkedIds as $id) {
                    $podcast = Podcast::findOrFail($id);
                    $podcast->delete();
                }
                Flash::success('Deleted Successfully');
            }
        }
        catch (\Exception $e) {
            Flash::error($e->getMessage());
        }
        return $this->listRefresh();
    }

}