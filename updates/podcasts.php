<?php namespace Monologophobia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Podcasts extends Migration {

    public function up() {


        Schema::create('mono_podcasts', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('backend_users')->onDelete('cascade');
            $table->string('title');
            $table->string('image')->nullable();
            $table->text('description')->nullable();
            $table->date('published_at');
            $table->string('explicit');
            $table->text('keywords')->nullable();
            $table->timestamps();
        });

    }

    public function down() {
        Schema::dropIfExists('mono_podcasts');
    }

}